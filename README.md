![Curriculum](https://gitlab.com/davidalejandrola/ufps_css/-/raw/master/imagenes/imgreadme.PNG)
# Título del proyecto:

#### Ejercicio CSS/UFPS Curriculum Vitae
***

## Índice

1. [Características](#características-)✨
2. [Contenido del proyecto](#contenido-del-proyecto-)📋
3. [Tecnologías](#tecnologías-)⭐
4. [IDE](#ide-)🌎
5. [Instalación](#instalación-)🔨
6. [Demo](#demo-)🎮
7. [Autor(es)](#autores-)🏅
8. [Institución Académica](#institución-académica-)📒
9. [Referencias](#referencias-)☝
***

#### Características ✨: 

  - Proyecto de hoja de vida o Curriculum Vitae
  - Proyecto realizado con CSS y HTML 
  - Despliegue continuo en GitLab
  
***
#### Contenido del proyecto 📋
  

| Nombre del archivo 	| Ruta del archivo                                                                   	| Descripción                          	|
|--------------------	|------------------------------------------------------------------------------------	|--------------------------------------	|
| index.html         	| https://gitlab.com/davidalejandrola/ufps_css/-/blob/master/index.html              	| pagina principal del proyecto        	|
| imagenes           	| https://gitlab.com/davidalejandrola/ufps_css/-/tree/master/imagenes                	| Carpeta de las ímagenes del proyecto 	|
| html               	| https://gitlab.com/davidalejandrola/ufps_css/-/tree/master/html                    	| Carpeta de los HTML del proyecto     	|
| education.html     	| https://gitlab.com/davidalejandrola/ufps_css/-/blob/master/html/education.html     	| Listado de educación                 	|
| proyect.html       	| https://gitlab.com/davidalejandrola/ufps_css/-/blob/master/html/proyect.html       	| Lista de proyectos                   	|
| skillacademic.html 	| https://gitlab.com/davidalejandrola/ufps_css/-/blob/master/html/skillacademic.html 	| Lista de habilidades academicas      	|
| skillpersonal.html 	| https://gitlab.com/davidalejandrola/ufps_css/-/blob/master/html/skillpersonal.html 	| Lista de habilidades personales      	|
| sumarry.html       	| https://gitlab.com/davidalejandrola/ufps_css/-/blob/master/html/sumarry.html       	| Resumen personal                     	|





***
#### Tecnologías ⭐

Se lleva acabo una plantilla llamada [UFPS_CSS](#referencias-) dada por el apreciado Ingeniero instructor de la materia Programación Web, grupo B del año 2021 semestre l. También se implemento teoría enseñada por el instructor de la materia. Como tecnologías se llevaron a acabo:
  
  - [![HTML5](https://img.shields.io/badge/HTML5-CSS-green)](https://developer.mozilla.org/es/docs/Web/Guide/HTML/HTML5)
  
   ![CSSandHTML5](https://www.codigosinformaticos.com/wp-content/uploads/2019/04/html-y-cssb-.jpg)

***
#### IDE  🌎

- El proyecto se desarrolla usando Sublime Text 3


  ![SublimeText3](http://tomasdelvechio.github.io/img/blog/2018/logo-sublime-text-3.png)


- Se usa el navegador Chrome para realizar las pruebas


  ![Chrome](http://1.bp.blogspot.com/-q8PytfYcbg8/Ty7eOLtALtI/AAAAAAAAFI8/ykMOUcs_f48/s200/Chrome+logo.png)

***
### Instalación    🔨

Se usa un navegador compatible con HTML5


```sh
-Descargar proyecto 
-Invocar página index.html desde Chrome 
```

***
### Demo  🎮

-Despliegue continuo en servidor de GitLab:  [Despliegue](https://davidalejandrola.gitlab.io/ufps_css)

-Para ver el codigo de la aplicación puede dirigirse a: [Descargar la aplicación web en archivo .zip](https://gitlab.com/davidalejandrola/ufps_css/-/archive/master/ufps_css-master.zip).

***
### Autor(es)  🏅
Proyecto desarrollado por:
  
  
  - Alejandro López (<davidalejandrola@ufps.edu.co>).


***
### Institución Académica  📒  
Proyecto desarrollado en la Materia programación web para una actividad académica del  [Programa de Ingeniería de Sistemas] de la [Universidad Francisco de Paula Santander]


   
   [Programa de Ingeniería de Sistemas]:<https://ingsistemas.cloud.ufps.edu.co/>
   [Universidad Francisco de Paula Santander]:<https://ww2.ufps.edu.co/>


#### Referencias  ☝

[Plantilla usada UFPS_CSS](https://gitlab.com/programacion-web---i-sem-2019/ufps_css)

